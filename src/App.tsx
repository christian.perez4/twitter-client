import { UserProvider } from './context/UserContext';
import AppRouter from './routers/AppRouter';
import GlobalStyles from './styles/GlobalStyles';
import { ThemeProvider } from 'styled-components';
import { appTheme } from './theme/appTheme';

function App() {
  return (
    <div>
      <ThemeProvider theme={appTheme}>
        <UserProvider>
          <GlobalStyles />
          <AppRouter />
        </UserProvider>
      </ThemeProvider>
    </div>
  );
}

export default App;
