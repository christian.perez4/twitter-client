import styled from 'styled-components';

export const ContainerMain = styled.div`
  padding-top: 78px;
  height: 100vh;
`;

export const Container = styled.div`
  padding: 20px;
`;

export const Button = styled.button`
  padding: 12px 12px;
  margin: 10px auto 0;
  background-color: ${({ theme }) => theme.secondary};
  border: 0;
  border-radius: 4px;
  color: black;
  transition: all 0.5s ease;

  &:hover {
    cursor: pointer;
    opacity: 0.9;
    transform: scale(0.98);
  }

  &:active {
    transform: scale(1.05);
    transition: all 0.5s ease;
  }
`;

export const ButtonEffect = styled.button`
  box-sizing: border-box;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  background-color: transparent;
  border: 2px solid #3498db;
  border-radius: 0.6em;
  cursor: pointer;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-align-self: center;
  -ms-flex-item-align: center;
  align-self: center;
  font-size: 1rem;
  font-weight: 400;
  line-height: 1;
  margin-top: 2rem;
  padding: 1.2em 2.8em;
  text-decoration: none;
  text-align: center;
  text-transform: uppercase;
  font-weight: 700;
  color: #fff;
  box-shadow: 0 0 40px 40px #3498db inset, 0 0 0 0 #3498db;
  -webkit-transition: all 150ms ease-in-out;
  transition: all 150ms ease-in-out;
  &:hover {
    box-shadow: 0 0 10px 0 #3498db inset, 0 0 10px 4px #3498db;
  }
`;

export const InputField = styled.input`
  padding: 10px 0 10px 15px;
  margin: 15px auto;
  border-radius: 4px;
  border: 0.5px solid rgba(0, 0, 0, 0.25);
  background-color: rgba(255, 255, 255, 1);
  font-size: 14px;

  &:focus {
    outline: none;
    box-shadow: 0 0 0 1pt ${({ theme }) => theme.primary};
  }
`;

export const Title = styled.h4`
  text-align: center;
  padding: 5px 0;
  color: ${({ theme }) => theme.primary};
`;

export const Subtitle = styled.h1`
  text-align: center;
  padding: 5px 0;
  color: ${({ theme }) => theme.textColor};
`;
