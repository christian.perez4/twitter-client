import { useContext } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate
} from 'react-router-dom';
import HomeScreen from '../features/Home/pages/HomeScreen';
import AboutScreen from '../features/About/pages/AboutScreen';
import UserScreen from '../features/User/pages/UserScreen';
import MyProfileScreen from '../features/User/pages/MyProfileScreen';
import { UserContext } from '../context/UserContext';
import LoginScreen from '../features/Login/pages/LoginScreen';
import styled from 'styled-components';
import Navbar from '../features/Navbar/components/Navbar';
import MyFollowersScreen from '../features/User/pages/MyFollowersScreen';
import { appTheme } from '../theme/appTheme';

const AppRouter = () => {
  const { nickname } = useContext(UserContext);

  const ContainerRouter = styled.div`
    height: 100%;
    background: linear-gradient(
      to right bottom,
      ${appTheme.background},
      ${appTheme.contrast}
    );
  `;

  return (
    <ContainerRouter>
      <Router>
        <Navbar />
        <Routes>
          {nickname ? (
            <>
              <Route path="/" element={<HomeScreen />} />
              <Route path="/followers" element={<MyFollowersScreen />} />
              <Route path="/myprofile" element={<MyProfileScreen />} />
              <Route path="/about" element={<AboutScreen />} />
              <Route path="/users/:id" element={<UserScreen />} />
            </>
          ) : (
            <>
              <Route path="/" element={<LoginScreen />} />
              <Route path="/about" element={<AboutScreen />} />
              <Route path="*" element={<Navigate to="/" replace />} />
            </>
          )}
        </Routes>
      </Router>
    </ContainerRouter>
  );
};

export default AppRouter;
