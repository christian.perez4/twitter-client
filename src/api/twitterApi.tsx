import axios, { AxiosResponse } from 'axios';

axios.defaults.baseURL = 'http://localhost:4000';

const responseBody = (response: AxiosResponse) => response.data;

const requests = {
  get: (url: string) => axios.get(url).then(responseBody),
  post: (url: string, body: {}) => axios.post(url, body).then(responseBody)
};

const Users = {
  registerUser: (nickname: string, name: string): Promise<any> =>
    requests.post('/registeruser', {
      name,
      nickname
    }),
  updateUser: (nickname: string, newName: string): Promise<any> =>
    requests.post('/updatename', {
      nickname,
      newName
    })
};

const Follows = {
  followUser: (
    nicknameFollower: string,
    nicknameToFollow: string
  ): Promise<any> =>
    requests.post('/followuser', {
      nicknameFollower,
      nicknameToFollow
    }),
  getFollowers: (nickname: string): Promise<any> =>
    requests.get(`/followers/${nickname}`)
};

const Tweets = {
  makeTweet: (nickname: string, message: string): Promise<any> =>
    requests.post('/maketweet', {
      nickname,
      message
    }),
  getTweets: (nickname: string): Promise<any> =>
    requests.get(`/tweets/${nickname}`)
};

export const twitterApi = axios.create();

export default {
  Users,
  Follows,
  Tweets
};
