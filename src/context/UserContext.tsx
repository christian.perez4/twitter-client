import { createContext, useState } from 'react';
import twitterApi from '../api/twitterApi';

interface IUserContext {
  nickname: string;
  name: string;
  setNickname: (nickname: string) => void;
  setName: (name: string) => void;
  registerUser: (nickname: string, name: string) => void;
}

export const UserContext = createContext({} as IUserContext);

export const UserProvider = ({ children }: any) => {
  const [nickname, setNicknameLocal] = useState('');
  const [name, setNameLocal] = useState('');

  const setNickname = (nickname: string) => {
    setNicknameLocal(nickname);
  };

  const setName = (nickname: string) => {
    setNameLocal(nickname);
  };

  const registerUser = async (nickname: string, name: string) => {
    try {
      await twitterApi.Users.registerUser(nickname, name);

      setNicknameLocal(nickname);
      setNameLocal(name);
    } catch (error) {
      setNicknameLocal(nickname);
    }
  };

  return (
    <UserContext.Provider
      value={{
        nickname,
        name,
        setNickname,
        setName,
        registerUser
      }}
    >
      {children}
    </UserContext.Provider>
  );
};
