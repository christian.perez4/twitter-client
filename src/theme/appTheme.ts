export const appTheme = {
    primary: '#46c7f0',
    secondary: '#72d391',
    terciary: '#333333',
    fourth: '#4b4b55',
  
    background: '#141414',
    contrast: '#333333',
  
    textColor: '#fff',
    textHighlightColor: '#000',
  };