import { useContext, useEffect, useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import twitterApi from '../../../api/twitterApi';
import { UserContext } from '../../../context/UserContext';
import {
  Button,
  ButtonEffect,
  Container,
  InputField
} from '../../../styles/Common.styled';
import ScrollToTopButton from '../../Navbar/components/ScrollToTop';
import Tweet from '../components/Tweet';
import {
  Container as ContainerHome,
  ContainerMakeTweet,
  InputMessageTweet
} from '../styles/HomeScreen.styled';

const HomeScreen = () => {
  const { nickname } = useContext(UserContext);

  const [messageTweet, setMessageTweet] = useState('');
  const [nicknameToFollow, setNicknameToFollow] = useState('');
  const [tweets, setTweets] = useState([]);

  const notify = (message: string) => toast(message);

  const handleClickMakeTweet = async () => {
    if (messageTweet) {
      await twitterApi.Tweets.makeTweet(nickname, messageTweet);
      setMessageTweet('');
      const tweets = await twitterApi.Tweets.getTweets(nickname);
      setTweets(tweets);
      notify('Tweet has been published!');
    }
  };

  const handleClickFollowUser = async () => {
    if (nicknameToFollow && nickname !== nicknameToFollow) {
      await twitterApi.Follows.followUser(nickname, nicknameToFollow);
      setNicknameToFollow('');
      notify('User has been followed!');
    }
  };

  useEffect(() => {
    const getMyTweets = async () => {
      const tweets = await twitterApi.Tweets.getTweets(nickname);
      setTweets(tweets);
    };

    getMyTweets();
  }, [nickname]);

  return (
    <ContainerHome>
      <ScrollToTopButton />
      <Container>
        <ContainerMakeTweet>
          <InputMessageTweet
            value={messageTweet}
            placeholder="What's happening?"
            onChange={(e: any) => setMessageTweet(e.target.value)}
          />
          <ButtonEffect onClick={handleClickMakeTweet}>
            Publish Tweet
          </ButtonEffect>
        </ContainerMakeTweet>

        <InputField
          type="text"
          placeholder="Insert nickname"
          value={nicknameToFollow}
          onChange={(e: any) => setNicknameToFollow(e.target.value)}
        />
        <Button onClick={handleClickFollowUser}>Follow User</Button>
      </Container>
      <div>
        {tweets &&
          tweets.map((t, index) => (
            <Tweet key={index} nickname={nickname} message={t} />
          ))}
      </div>
      <ToastContainer />
    </ContainerHome>
  );
};

export default HomeScreen;
