import styled from 'styled-components';

export const Container = styled.div`
    padding: 20px;
    padding-top: 78px;
`;

export const ContainerMakeTweet = styled.div`
    display: flex;
    flex-direction: column;
`;

export const InputMessageTweet = styled.textarea`
    width: 100%;
    height: 100px;
    padding: 10px 0 10px 15px;
    border-radius: 4px;
    border: 0.5px solid rgba(0, 0, 0, 0.25);
    background-color: rgba(255, 255, 255, 1);
    font-size: 18px;
    font-family: Urbanist;

    &:focus {
        outline: none;
        box-shadow: 0 0 0 1pt ${({ theme }) => theme.primary};
    }
`;