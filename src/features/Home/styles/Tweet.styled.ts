import styled from 'styled-components';
import { appTheme } from '../../../theme/appTheme';

export const Container = styled.div`
    background: linear-gradient(to right bottom, ${appTheme.primary}, ${appTheme.contrast});
    margin: 20px;
    padding: 10px;
    border-radius: 10px;
`;

export const Nickname = styled.p`
    margin-bottom: 5px;
    font-weight: bold;
`;