import { Container, Nickname } from '../styles/Tweet.styled';

interface IProps {
  nickname: string;
  message: string;
}

const Tweet = ({ nickname, message }: IProps) => {
  return (
    <Container>
      <Nickname>@{nickname}</Nickname>
      <p>{message}</p>
    </Container>
  );
};

export default Tweet;
