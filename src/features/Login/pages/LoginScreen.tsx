import { useContext, useState } from 'react';
import { UserContext } from '../../../context/UserContext';
import { Button } from '../../../styles/Common.styled';
import {
  ContainerControls,
  ContainerLogin,
  InputField,
  Legend
} from '../styles/LoginScreen.styled';

const LoginScreen = () => {
  const [nickname, setNickname] = useState('');
  const [name, setName] = useState('');

  const { registerUser } = useContext(UserContext);

  const handleSubmit = (e: any) => {
    e.preventDefault();
    if (nickname && name) {
      registerUser(nickname, name);
    }
  };

  return (
    <ContainerLogin>
      <form onSubmit={handleSubmit}>
        <ContainerControls>
          <Legend>Nickname</Legend>
          <InputField
            type="text"
            value={nickname}
            onChange={(e: any) => setNickname(e.target.value)}
          />
          <Legend>Real name</Legend>
          <InputField
            type="text"
            value={name}
            onChange={(e: any) => setName(e.target.value)}
          />
          <Button>Login/Register</Button>
        </ContainerControls>
      </form>
    </ContainerLogin>
  );
};

export default LoginScreen;
