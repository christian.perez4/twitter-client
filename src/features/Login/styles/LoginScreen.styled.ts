import styled from 'styled-components';
import { appTheme } from '../../../theme/appTheme';
import backgroundImage from '../../../assets/images/office4.jpeg';

export const ContainerLogin = styled.div`
    height: 100vh;
    background: rgba(0, 0, 0, 0.7) url(${backgroundImage});
    background-size: 100% 100%;
    background-blend-mode: darken;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const ContainerControls = styled.div`
    display: flex;
    flex-direction: column;
`;

export const Legend = styled.p`

`;

export const InputField = styled.input`
    padding: 10px 0 10px 15px;
    display: block;
    margin: 15px auto;
    border-radius: 4px;
    border: 0.5px solid rgba(0, 0, 0, 0.25);
    background-color: rgba(255, 255, 255, 1);
    font-size: 14px;

    &:focus {
        outline: none;
        box-shadow: 0 0 0 1pt ${appTheme.primary};
    }
`;