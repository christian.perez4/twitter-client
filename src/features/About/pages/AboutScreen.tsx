import { MdEmail } from 'react-icons/md';
import {
  ContactLink,
  ContactTextLink,
  ContainerDescription,
  ContainerFeedback,
  Description,
  DescriptionFeedback
} from '../styles/AboutScreen.styled';
import {
  Container,
  Title,
  Subtitle,
  ContainerMain
} from '../../../styles/Common.styled';

const AboutScreen = () => {
  return (
    <ContainerMain>
      <Container>
        <Title>About</Title>
        <Subtitle>Kata Twitter</Subtitle>
      </Container>
      <ContainerDescription>
        <Description>This kata created by Christian Perez.</Description>
        <Description>
          Technologies used: React JS, Styled components, React Router.
        </Description>
      </ContainerDescription>

      <ContainerFeedback>
        <DescriptionFeedback>
          Please send me your feedback here:
        </DescriptionFeedback>
        <ContactLink href="mailto:christian.perez@etermax.com" target="_blank">
          <MdEmail />
          <ContactTextLink>christian.perez@etermax.com</ContactTextLink>
        </ContactLink>
      </ContainerFeedback>
    </ContainerMain>
  );
};

export default AboutScreen;
