import styled from 'styled-components';

export const ContainerDescription = styled.div`
  margin: 40px 5%;
`;

export const ContainerFeedback = styled.div`
  background-color: ${({ theme }) => theme.primary};
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 40px 5%;
`;

export const Description = styled.p`
  font-size: 18px;
  color: ${({ theme }) => theme.textColor};
  margin: 5px 0;
`;

export const DescriptionFeedback = styled.p`
  font-size: 18px;
  color: ${({ theme }) => theme.textHighlightColor};
  margin-right: 5px;
`;

export const DescriptionEmail = styled.p`
  font-size: 18px;
  color: ${({ theme }) => theme.textHighlightColor};
  margin-left: 5px;
`;

export const ContactLink = styled.a`
  text-decoration: none;
  color: ${({ theme }) => theme.textHighlightColor};
  display: flex;
  align-items: center;
  transition: all 0.5s ease;
  &:hover {
    color: rgba(255, 255, 255, 0.7);
    transition: all 0.5s ease;
  }
`;
export const ContactTextLink = styled.div`
  margin-left: 0.4rem;
  display: flex;
  align-items: center;
  font-size: 18px;
`;