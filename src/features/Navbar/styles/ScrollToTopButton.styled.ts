import styled, { keyframes } from 'styled-components';
import { FaAngleUp } from 'react-icons/fa';

const moveBtnAnimation = keyframes`
    0%{
        transform: translateY(0px);
    }
    50%{
        transform: translateY(20px);
    }
    100%{
        transform: translateY(0px);
    }
`;

export const ScrollButtonContainer = styled.div`
  position: relative;
`;

export const ScrollButtonIcon = styled(FaAngleUp)`
  position: fixed;
  bottom: 40px;
  right: 25px;
  z-index: 20;
  background-color: ${({ theme }) => theme.primary};
  border: 2px solid ${({ theme }) => theme.contrast};
  border-radius: 50%;
  height: 50px;
  width: 50px;
  color: ${({ theme }) => theme.textHighlightColor};
  cursor: pointer;
  animation: 3s ease-in-out infinite;
  transition: all 0.5s ease-in-out;
  animation-name: ${moveBtnAnimation};
  animation-iteration-count: infinite;
  &:hover {
    background: ${({ theme }) => theme.contrast};
    color: ${({ theme }) => theme.primary};
    border: 2px solid ${({ theme }) => theme.primary};
  }
`;