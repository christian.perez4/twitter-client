import styled from 'styled-components';

type PropsContainerNav = {
    showBackground: boolean
}

type PropsMenu = {
    isOpen: boolean
}

export const ContainerNav = styled.div<PropsContainerNav>`
  width: 100%;
  height: 78px;
  background-color: ${(props: any) => (props.showBackground ? props.theme.background : 'transparent')};
  transition: 0.5s ease-in-out;
  padding: 0 10px;
  position: fixed;
`;

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: auto;
`;

export const LogoContainer = styled.div`
  margin-left: 0.5rem;
  display: flex;
  align-items: center;
  font-size: 1.2rem;
  font-family: sans-serif;
  cursor: pointer;
  p {
    &:nth-child(2) {
      color: ${({ theme }) => theme.textColor};
    }
  }
  svg {
    fill: ${({ theme }) => theme.primary};
    margin-right: 0.5rem;
  }
`;

export const NameCompany = styled.p`
  letter-spacing: 4px;
  transition: all 0.5s ease;
  &:hover {
    letter-spacing: 8px;
    transition: all 0.5s ease;
  }
`;

export const Menu = styled.ul<PropsMenu>`
  height: 100%;
  display: flex;
  justify-content: space-between;
  list-style: none;
  @media screen and (max-width: 768px) {
    background-color: ${({ theme }) => theme.terciary};
    position: absolute;
    top: 70px;
    left: ${({ isOpen }: any) => (isOpen ? '0' : '-100%')};
    width: 100%;
    height: 90vh;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    transition: 0.5s all ease;
  }
`;

export const MenuItem = styled.li`
  height: 100%;
  @media screen and (max-width: 768px) {
    width: 100%;
    height: 70px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

export const MenuItemLink = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  height: 100%;
  padding: 0.5rem 1.5rem;
  color: ${({ theme }) => theme.primary};
  font-family: sans-serif;
  font-size: 0.8rem;
  font-weight: 300;
  cursor: pointer;
  transition: 0.5s all ease;
  &:hover {
    background-color: transparent;
    color: ${({ theme }) => theme.textColor};
    transition: 0.5s all ease;
    div {
      svg {
        fill: ${({ theme }) => theme.fourth};
      }
    }
    @media screen and (min-width: 1200px) {
      transform: translate3d(0, -2px, 0);
    }
    @media screen and (max-width: 1200px) {
    }
    @media screen and (max-width: 992px) {
    }
    @media screen and (max-width: 768px) {
      background-color: ${({ theme }) => theme.primary};
      color: ${({ theme }) => theme.fourth};
    }
    @media screen and (max-width: 576px) {
    }
  }
  div {
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    svg {
      display: none;
      fill: ${({ theme }) => theme.primary};
      margin-right: 0.5rem;
    }
  }
  @media screen and (max-width: 992px) {
    padding: 0.5rem 1.2rem;
    font-size: 0.7rem;
  }
  @media screen and (max-width: 768px) {
    width: 100%;
    div {
      width: 30%;
      justify-content: left;
      svg {
        display: flex;
      }
    }
  }
  @media screen and (max-width: 500px) {
    div {
      width: 60%;
      justify-content: left;
      svg {
        display: flex;
      }
    }
  }
  @media screen and (max-width: 260px) {
    div {
      width: 100%;
      justify-content: left;
      svg {
        display: flex;
      }
    }
  }
`;

export const MobileIcon = styled.div`
  display: none;
  @media screen and (max-width: 768px) {
    display: flex;
    align-items: center;
    cursor: pointer;
    svg {
      fill: ${({ theme }) => theme.primary};
      margin-right: 0.5rem;
    }
  }
`;