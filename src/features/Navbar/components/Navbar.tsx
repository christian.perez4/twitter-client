import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import {
  FaTwitter,
  FaBars,
  FaTimes,
  FaHome,
  FaUserAlt,
  FaBriefcase
} from 'react-icons/fa';
import { IconContext } from 'react-icons';
import {
  ContainerNav,
  LogoContainer,
  Menu,
  MenuItem,
  MenuItemLink,
  MobileIcon,
  NameCompany,
  Wrapper
} from '../styles/Navbar.styled';

const Navbar = () => {
  const navigate = useNavigate();
  const [showMobileMenu, setShowMobileMenu] = useState(false);
  const [showNavbarBackgroundColor, setShowNavbarBackgroundColor] =
    useState(false);

  const [menuItems] = useState([
    {
      description: 'HOME',
      icon: <FaHome />,
      goTo: '/'
    },
    {
      description: 'MY FOLLOWERS',
      icon: <FaUserAlt />,
      goTo: '/followers'
    },
    {
      description: 'MY PROFILE',
      icon: <FaUserAlt />,
      goTo: '/myprofile'
    },
    {
      description: 'ABOUT',
      icon: <FaBriefcase />,
      goTo: '/about'
    }
  ]);

  const changeNavBackgroundColor = () => {
    if (window.scrollY >= 90) {
      setShowNavbarBackgroundColor(true);
    } else {
      setShowNavbarBackgroundColor(false);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', changeNavBackgroundColor);

    return () => {
      window.removeEventListener('scroll', changeNavBackgroundColor);
    };
  }, []);

  return (
    <ContainerNav showBackground={showNavbarBackgroundColor}>
      <Wrapper>
        <IconContext.Provider value={{ style: { fontSize: '2em' } }}>
          <LogoContainer onClick={() => navigate('/')}>
            <FaTwitter />
            <NameCompany>Twitter</NameCompany>
          </LogoContainer>

          <MobileIcon onClick={() => setShowMobileMenu(!showMobileMenu)}>
            {showMobileMenu ? <FaTimes /> : <FaBars />}
          </MobileIcon>

          <Menu isOpen={showMobileMenu}>
            {menuItems.map((item) => (
              <MenuItem key={item.description}>
                <MenuItemLink
                  onClick={() => {
                    setShowMobileMenu(!showMobileMenu);
                    navigate(item.goTo);
                  }}
                >
                  <div>
                    {item.icon}
                    {item.description}
                  </div>
                </MenuItemLink>
              </MenuItem>
            ))}
          </Menu>
        </IconContext.Provider>
      </Wrapper>
    </ContainerNav>
  );
};

export default Navbar;
