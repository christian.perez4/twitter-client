import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import twitterApi from '../../../api/twitterApi';
import {
  Container,
  ContainerMain,
  Subtitle,
  Title
} from '../../../styles/Common.styled';
import Tweet from '../../Home/components/Tweet';

const UserScreen = () => {
  const { id } = useParams();
  const [tweets, setTweets] = useState([]);

  useEffect(() => {
    const getTweets = async () => {
      if (id) {
        const tweets = await twitterApi.Tweets.getTweets(id);
        setTweets(tweets);
      }
    };

    getTweets();
  }, [id]);

  return (
    <ContainerMain>
      <Container>
        <Title>User</Title>
        <Subtitle>@{id}</Subtitle>
      </Container>
      <Container>
        {tweets &&
          id &&
          tweets.map((t, index) => (
            <Tweet key={index} nickname={id} message={t} />
          ))}
      </Container>
    </ContainerMain>
  );
};

export default UserScreen;
