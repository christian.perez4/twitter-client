import { useContext, useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ContainerForm, ContainerMain } from '../styles/MyProfileScreen.styled';
import {
  Container,
  Title,
  Subtitle,
  InputField,
  Button
} from '../../../styles/Common.styled';
import { UserContext } from '../../../context/UserContext';
import twitterApi from '../../../api/twitterApi';

const MyProfileScreen = () => {
  const { nickname } = useContext(UserContext);
  const [newName, setNewName] = useState('');
  const { name, setName } = useContext(UserContext);

  const notify = (message: string) => toast(message);

  const handleClickUpdateName = async () => {
    if (newName) {
      await twitterApi.Users.updateUser(nickname, newName);
      setNewName('');
      setName(newName);
      notify('Your real name has been updated');
    }
  };

  return (
    <ContainerMain>
      <Container>
        <Title>My Profile</Title>
        <Subtitle>@{nickname}</Subtitle>
      </Container>
      <ContainerForm>
        {name && <p>Current name: {name}</p>}
        <InputField
          type="text"
          placeholder="New real name"
          value={newName}
          onChange={(e: any) => setNewName(e.target.value)}
        />
        <Button onClick={handleClickUpdateName}>Update</Button>
      </ContainerForm>
      <ToastContainer />
    </ContainerMain>
  );
};

export default MyProfileScreen;
