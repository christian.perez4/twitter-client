import { useContext, useEffect, useState } from 'react';
import { ContainerForm, ContainerMain } from '../styles/MyProfileScreen.styled';
import { Container, Title, Subtitle } from '../../../styles/Common.styled';
import { UserContext } from '../../../context/UserContext';
import twitterApi from '../../../api/twitterApi';
import { useNavigate } from 'react-router-dom';
import { MenuItemLink } from '../../Navbar/styles/Navbar.styled';

const MyFollowersScreen = () => {
  const { nickname } = useContext(UserContext);
  const [followers, setFollowers] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const getMyFollowers = async () => {
      const followers = await twitterApi.Follows.getFollowers(nickname);
      setFollowers(followers);
    };

    getMyFollowers();
  }, [nickname]);

  return (
    <ContainerMain>
      <Container>
        <Title>My Followers</Title>
        <Subtitle>@{nickname}</Subtitle>
      </Container>
      <ContainerForm>
        <div>
          <strong>Your followers</strong>
          {followers &&
            followers.map((t, index) => (
              <MenuItemLink
                key={index}
                onClick={() => {
                  navigate(`/users/${t}`);
                }}
              >
                <div>@{t}</div>
              </MenuItemLink>
            ))}
        </div>
      </ContainerForm>
    </ContainerMain>
  );
};

export default MyFollowersScreen;
