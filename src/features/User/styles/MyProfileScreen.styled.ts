import styled from 'styled-components';

export const ContainerMain = styled.div`
  padding-top: 78px;
  height: 100vh;
`;

export const ContainerForm = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding-top: 50px;
`;